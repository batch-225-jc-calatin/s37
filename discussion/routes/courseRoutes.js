const express = require("express");
const router = express.Router();
const auth = require("../auth");

const courseController = require("../controllers/courseController");


// Router for creating a course

router.post("/addCourse", auth.verify, (req, res) => {

	const data = {
		course : req.body,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	courseController.addCourse(data).then(result => res.send(result));
});

// Activity
// Controller function that retrieving all the courses



// route for retrieving all

// route for ADmin
router.get("/all", (req, res) => {
	courseController.getAllCourse().then(resultFromController => res.send(resultFromController));
});


// Route for Updating the course

router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(result => res.send(result));
});




// routes for Archive
// using params
// soft Delete

// use unarchive for reverse result

router.put("/archive/:courseId", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params, req.body).then(result => res.send(result));
});



// routes for users in getting courses



router.get("/getAllActive", auth.verify, (req, res) => {
	courseController.getAllActive().then(result => res.send(result));
})



/*
	Plan for capstone :


log in page for

	Admin - manage - (product - All - Both Active and In Active

	users - buy - (product - Limited -


*/






module.exports = router;