const jwt = require("jsonwebtoken");
// User defined string data that will be used to create our JSON web tokens
// Used in the algorithm for encrypting our data which makes it difficult to decode the information without the defined secret keyword

const dotenv = require("dotenv").config();


const secret = process.env.SECRET




module.exports.createAccessToken = (user) => {

	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin,

	}

	// Generate a JSON web token using the jwt's sign method
	// Generates the token using the form data and the secret code with no additional options provided

	return jwt.sign(data, secret, {
		// expiresIn : 60 - for session timeout (for fields work only )
	});
};

// Token Verification

/*
- Analogy
	Receive the gift and open the lock to verify if the the sender is legitimate and the gift was not tampered with
*/



// The token is retrieved from the request header
	// This can be provided in postman under
		// Authorization > Bearer Token

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if (typeof token !== "undefined") {
		console.log(token);


		// The "slice" method takes only the token from the information sent via the request header
		// The token sent is a type of "Bearer" token which when recieved contains the word "Bearer " as a prefix to the string
		// This removes the "Bearer " prefix and obtains only the token for verification

		token = token.slice(7, token.length);


		// Validate the token using the "verify" method decrypting the token using the secret code

		// Decryption is a process that transforms encrypted information into its original format

		return jwt.verify(token, secret, (err, data) => {

			if (err) {
				return res.send({auth : "failed"})
			} else {

				// Allows the application to proceed with the next middleware function/callback function in the route
				// The verify method will be used as a middleware in the route to verify the token before proceeding to the function that invokes the controller function

				next()
			}
		})



	} else {
		return res.send({auth :"failed"});
	};


};

// Token Decryption

/*
	- Analogy
		Open the gift and get the content

*/


module.exports.decode = (token) => {
	// code

	if (typeof token !== "undefined") {
		console.log(token);


		
		token = token.slice(7, token.length);


		

		return jwt.verify(token, secret, (err, data) => {

			if (err) {

				return null;
			} else {
				return jwt.decode(token, {complete : true}).payload;
			}

})
	} else {
		return null;
	}
};




// next na middleware which si module.exports.decode yung irurun